/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  }
  if (sporocilo.match(new RegExp(/(\bhttp\S+\.(?:png\b|jpg\b|gif\b))/gi))) {
    return divElementHtmlTekst(sporocilo);
  }
  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sporociloKopija = sporocilo;
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    var besede = sporocilo.split(' ');
    besede.shift();
    var besedilo = besede.join(' ');
    var parametri = besedilo.split('\"');
    
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    /*if (sistemskoSporocilo[0] == '/preimenuj ') {
      
    }*/
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      if (sporociloKopija.match(new RegExp(/(\bhttp\S+\.(?:png\b|jpg\b|gif\b))/gi))) {
         sistemskoSporocilo = vrniNasloveSlik(sistemskoSporocilo);
         $('#sporocila').append(sistemskoSporocilo);
         socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: sistemskoSporocilo});
      }
    }
  }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    if (sporociloKopija.match(new RegExp(/(\bhttp\S+\.(?:png\b|jpg\b|gif\b))/gi))) {
      sporocilo = vrniNasloveSlik(sporocilo);
      klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
      $('#sporocila').append(sporocilo);
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var uporabniki = [];

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }
    
    $('#seznam-uporabnikov div').click(function() {
      console.log("Klik deluje " + $(this).text());
      socket.emit('sporocilo', {vzdevek: $(this).text(), besedilo: '☜'});
     $('#sporocila').append('(zasebno za ' + $(this).text() + '): ' + '☜');
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });

});


function vrniNasloveSlik(vhod, index) {
  index || (index = 1); //default index je 1
  var slike = [];
  var match;
  var expression = /(\bhttp\S+\.(?:png\b|jpg\b|gif\b))/gi;
  while (match = expression.exec(vhod)) {
    slike.push(match[index]);
  }
  for (var i = 0; i < slike.length; i++) {
    slike[i] = slike[i].split(slike[i]).join("<br /><img style='width: 200px; padding: 20px' src='" + slike[i] + "' />");
  }
  return slike;
}

